// Copyright (c) 2022 Hemashushu <hippospark@gmail.com>, All rights reserved.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

use anvm_ast::{ast::GlobalType, types::Value};

use crate::error::EngineError;

pub struct VMGlobalVariable {
    /// GlobalType 记录变量的 `数据类型` 以及 `可变性`
    global_type: GlobalType,

    value: Value,
}

impl VMGlobalVariable {
    pub fn new(global_type: GlobalType, value: Value) -> Self {
        VMGlobalVariable {
            global_type: global_type,
            value: value,
        }
    }

    pub fn get_value(&self) -> Value {
        self.value
    }

    pub fn set_value(&mut self, value: Value) -> Result<(), EngineError> {
        if !self.global_type.mutable {
            return Err(EngineError::InvalidOperation(
                "the specified global variable is immutable".to_string(),
            ));
        }

        if self.value.get_type() != value.get_type() {
            return Err(EngineError::InvalidOperation(
                "the type of the new value does not match the specified global variable"
                    .to_string(),
            ));
        }

        self.value = value;
        Ok(())
    }

    pub fn get_global_type(&self) -> &GlobalType {
        &self.global_type //.clone()
    }
}

#[cfg(test)]
mod tests {
    use anvm_ast::{
        ast::GlobalType,
        types::{Value, ValueType},
    };

    use crate::error::EngineError;

    use super::VMGlobalVariable;

    #[test]
    fn test_get_set() {
        // 创建一个不可变的全局变量
        let mut g0 = VMGlobalVariable::new(
            GlobalType {
                value_type: ValueType::I32,
                mutable: false,
            },
            Value::I32(10),
        );

        assert_eq!(g0.get_value(), Value::I32(10));
        assert!(matches!(
            g0.set_value(Value::I32(20)),
            Err(EngineError::InvalidOperation(_))
        ));

        // 创建一个可变的全局变量
        let mut g1 = VMGlobalVariable::new(
            GlobalType {
                value_type: ValueType::I32,
                mutable: true,
            },
            Value::I32(20),
        );

        assert_eq!(g1.get_value(), Value::I32(20));

        assert!(matches!(g1.set_value(Value::I32(30)), Ok(_)));
        assert_eq!(g1.get_value(), Value::I32(30));

        assert!(matches!(
            g1.set_value(Value::I64(40)), // 设置一个不同数据类型的值
            Err(EngineError::InvalidOperation(_))
        ))
    }
}
